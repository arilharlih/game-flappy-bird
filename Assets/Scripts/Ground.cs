﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Komponen ditambahkan jika tidak ada dan komponen tidak dapat dibuang
[RequireComponent(typeof(BoxCollider2D))]
public class Ground : MonoBehaviour
{
	// Global Variabel
	[SerializeField] private Bird bird;
	[SerializeField] private float speed = 1;
	[SerializeField] private Transform nextPos;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // pengecekan jika burung belum mati
        if (bird == null || (bird != null && !bird.IsDead())) {
        	// buat pipa gerak ke arah kiri dengan kecepatan variabel speed
        	transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
        }
    }

    // Method untuk menempatkan game object pada posisi next ground
    public void SetNextGround(GameObject ground)
    {
    	// Pengecekan Null Value
    	if (ground != null) {
    		// Menempatkan ground berikutnya pada nextground
    		ground.transform.position = nextPos.position;
    	}
    }

    // Dipanggil ketika game object bertemu dengan game object lain
    private void OnCollisionEnter2D(Collision2D collision)
    {
    	// Membuat burung mati saat bersentuhan dengan ini
    	if (bird != null && !bird.IsDead()) {
    		bird.Dead();
    	}
    }
}
