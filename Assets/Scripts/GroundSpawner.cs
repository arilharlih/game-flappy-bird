﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]

public class GroundSpawner : MonoBehaviour
{
	// Menampung referensi ground
	[SerializeField]
	private Ground groundRef;

	// Menampung Ground sebelumnya
	private Ground prevGround;

    // Method untuk membuat game object baru
    private void SpawnGround()
    {
    	// Pengecekan null variabel
    	if (prevGround != null) {
    		// Duplikat groundRef
    		Ground newGround = Instantiate(groundRef);

    		// Mengaktifkan game object
    		newGround.gameObject.SetActive(true);

    		// menempatkan new ground dengan posisi nextground dari prevground agar sejajar
    		prevGround.SetNextGround(newGround.gameObject);
    	}
    }

    // Method ini dipanggil saat terdapat komponen lain yang memiliki collider keluar dari area collider
    private void OnTriggerExit2D(Collider2D collision)
    {
    	// Mencari Component ground dari object yang keluar dari area trigger
    	Ground ground = collision.GetComponent<Ground>();

    	// Pengecekan null variabel
    	if (ground) {
    		// isi variabel prevGround
    		prevGround = ground;

    		// Buat ground baru
    		SpawnGround();
    	}
    }
}
