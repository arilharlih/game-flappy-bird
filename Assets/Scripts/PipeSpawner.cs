﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeSpawner : MonoBehaviour
{
	[SerializeField] private Bird bird;
	[SerializeField] private Pipe pipeUp, pipeDown;
	[SerializeField] private float spawnInterval = 1;
	[SerializeField] private float holeSize = 1f;
	[SerializeField] private float maxMinOffset = 1;
	[SerializeField] private Point point;

	// Variable penampng coroutine yang sedang berjalan
	private Coroutine CR_Spawn;

    // Start is called before the first frame update
    void Start()
    {
        // Mulai Spawn
        StartSpawn();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void StartSpawn()
    {
        // Menjalankan Fungsi Coroutine IeSpawn
        if (CR_Spawn == null) {
        	CR_Spawn = StartCoroutine(IeSpawn());
        }
    }

    void StopSpawn()
    {
    	// Menghentikan Coroutine IeSpawn jika sebelumnya sudah jalan
    	if (CR_Spawn != null) {
    		StopCoroutine(CR_Spawn);
    	}
    }

    void SpawnPipe()
    {
    	// Random Hole
    	holeSize = Random.Range(0.5f ,2.0f);

    	// Menduplikasi game object pipeUp dan menempatkan posisinya sama dengan game object ini tetapi dirotasi 180 derajat
    	Pipe newPipeUp = Instantiate(pipeUp, transform.position, Quaternion.Euler(0,0,180));

    	// aktifkan game object newPipeUp
    	newPipeUp.gameObject.SetActive(true);

    	// duplikasi game object pipeDown dan tempatkan posisi sama dengan game object
    	Pipe newPipeDown = Instantiate(pipeDown, transform.position, Quaternion.identity);

    	// aktifkan game object newPipeDown
    	newPipeDown.gameObject.SetActive(true);

    	// Menempatkan posisi dari pipa yang terbentuk agar memiliki lubang di tengah
    	newPipeUp.transform.position += Vector3.up * (holeSize / 2);
    	newPipeDown.transform.position += Vector3.down * (holeSize / 2);

    	// menempatkan posisi sesuai dengan fungsi sin
    	float y = maxMinOffset * Mathf.Sin(Time.time);
    	newPipeUp.transform.position += Vector3.up * y;
    	newPipeDown.transform.position += Vector3.up * y;

    	// Point
    	Point newPoint = Instantiate(point, transform.position, Quaternion.identity);
    	newPoint.gameObject.SetActive(true);
    	newPoint.SetSize(10.0f);
    	newPoint.transform.position += Vector3.up * y;
    }

    IEnumerator IeSpawn()
    {
    	while (true) {
    		// Jika burung mati maka menghentikan pembuatan pipa baru
    		if (bird.IsDead()) {
    			StopSpawn();
    		}

    		// Membuat Pipa Baru
    		SpawnPipe();

    		// Menunggu beberapa detik sesuai spawn internal
    		yield return new WaitForSeconds(spawnInterval);
    	}
    }
}
