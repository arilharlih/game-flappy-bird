﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void LoadScene(string name)
    {
    	// Cek jika name tidak null atau empty
		if (!string.IsNullOrEmpty(name)) {
			// buka scene sesuai nama variabel
			SceneManager.LoadScene(name);
		}
    }
}
